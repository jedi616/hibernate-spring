package keith.hibernate.spring.entity;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class Student
{
    private long studentId;
    private String name;

    public long getStudentId()
    {
        return studentId;
    }

    public void setStudentId(long studentId)
    {
        this.studentId = studentId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
