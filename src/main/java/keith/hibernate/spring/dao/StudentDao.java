package keith.hibernate.spring.dao;

import keith.domain.jpa.entity.StudentM;
import keith.hibernate.spring.dao.generic.GenericDao;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentDao extends GenericDao<StudentM, Long>
{
}
