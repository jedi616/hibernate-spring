package keith.hibernate.spring.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public abstract class GenericDaoImpl<E, ID extends Serializable> implements GenericDao<E, ID>
{
    @Autowired
    private SessionFactory sessionFactory;

    private Class<E> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDaoImpl()
    {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public List<E> findAll() throws Exception
    {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            Criteria criteria = session.createCriteria(entityClass);
            @SuppressWarnings("unchecked")
            List<E> entities = criteria.list();
            transaction.commit();
            return entities;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    public List<E> findAll(String hqlQuery, Map<String, Object> params) throws Exception
    {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            Query query = session.createQuery(hqlQuery);
            for (Map.Entry<String, Object> entry : params.entrySet())
            {
                String key = entry.getKey();
                Object value = entry.getValue();
                query.setParameter(key, value);
            }
            @SuppressWarnings("unchecked")
            List<E> entities = query.list();
            transaction.commit();
            return entities;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    public E findOne(ID id) throws Exception
    {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            E entity = session.get(entityClass, id);
            transaction.commit();
            return entity;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    public E save(E entity) throws Exception
    {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            @SuppressWarnings("unchecked")
            E savedEntity = (E) session.merge(entity);
            transaction.commit();
            return savedEntity;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    public void delete(ID id) throws Exception
    {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            E entityToDelete = session.get(entityClass, id);
            session.delete(entityToDelete);
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    public void truncateTable() throws Exception
    {
        System.out.println("GENERIC:");
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            Query query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 0");
            query.executeUpdate();

            Table tableAnnotation = entityClass.getAnnotation(Table.class);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("truncate table ");
            stringBuilder.append(tableAnnotation.name());
            query = session.createSQLQuery(stringBuilder.toString());
            query.executeUpdate();

            query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 1");
            query.executeUpdate();
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }
}
